const state = {
    session: {
        token: null,
        user: null
    }
}

const getters = {
    isAuthorized: (state) => state.session.token != null,
    getToken: (state) => state.session.token,
    getUser: (state) => state.session.user
}

const actions = {
    async setSession({ commit }, session) {
        commit('setSession', session)
    },
    async resetSession({commit}) {
        commit('setSession', {
            'token': null,
            'user': null,
        })
    },
}

const mutations = {
    setSession: (state, session) => state.session = session
}

export default {
    state, getters, actions, mutations
}