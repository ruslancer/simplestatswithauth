import Vuex from 'vuex'
import Vue from 'vue'
import session from './modules/session'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        session
    }
})