import LoginPage from "./pages/LoginPage"
import HomePage from "./pages/HomePage"

const routes = [
    { path: '/', component: HomePage },
    { path: '/login', component: LoginPage }
]

export default routes