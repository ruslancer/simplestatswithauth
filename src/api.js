import axios from 'axios'

const API = "http://www.5gmobilephones.co.uk/dashboard/api/"

export default {
    authorize(params) {
        var querystring = require('querystring')
        return axios.post(API + "authenticate", querystring.stringify(params))
    },

    logout(token) {
        return axios.get(API + "logout/" + token)
    },

    loadStats(token, params) {
        return axios.get(API + "stats/" + token + '?offset=' + params.offset + '&limit=' + params.limit)
    },

    loadVisitors(token, params) {
        return axios.get(API + "visitors/" + token + '?offset=' + params.offset + '&limit=' + params.limit)
    }
}